# Формы записи выражений

Инфиксная форма: "2 + 2 * 2" - как привыкли люди  
Обратная польская нотация (RPN): "2 2 2 * +" - как привыкли машины  

# Токенизация

**Токен** - логически неделимая последовательность символов в выражении (грубо говоря лексема), 
относящаяся к какому-то классу (оператор, число,...).  

**Токенизация** - процесс разбиения выражения на токены.  

```c++
"2 + 2 * 2" -> ["2", "+", "2", "*", "2"]
"10.5 + (23 - 555)" -> ["10.5", "+", "(", ....]
```

# Алгоритм сортировочной станции (Shunting Yard)

Вход: последовательность токенов математического выражения, записанного в инфиксной форме (["2", "+", "2", "*", "2"])  
Результат: очередь токенов, представленная в обратной польской нотации (RPN)  

## 1. Приоритет операторов
Определяет порядок применения операторов.  
Priority("^") > Priority("*") = Priority("/") > Priority("+") = Priority("-")  

## 2. Ассоциативность
Правила применения оператора к операндам  
"^" - правоассоциативный  

Ассоциативность: 2 + 3 + 4 = (2 + 3) + 4 = 2 + (3 + 4)  
Левая ассоциативность: 2 – 4 – 6 = (2 – 4) – 6 != 2 – (4 – 6)   
Правая ассоциативность: 2 ^ 3 ^ 4 = 2 ^ (3 ^ 4) != (2 ^ 3) ^ 4  

## 3. Количество операндов
Сколько операндов нужно оператору (также относится и к функциям: количество аргументов функции).  
Бинарные операторы: 2 – 3, 2 + 3, 2 * 3 …   
Унарные операторы: -1, ~5, …  


## Сущности, используемые в алгоритме:
1. стек операторов;
2. выходная очередь.

## Алгоритм:

```c++

// Number - число
// Function - функция
// Operator - оператор

output = Queue()
operatorStack = Stack()

for token in tokens:
    if token is Number:
        output.enqueue(token)
    if token is Function:
        operatorStack.push() // sin, cos
    if token is Operator:
        while (
            not operatorStack.isEmpty() AND 
            operator.top() != '(' AND 
            (
                operatorStack.top() is Function OR
                priority(operatorStack.top()) > priority(token) OR
                (priority(operatorStack.top()) == priority(token) AND associativity(token) == Left) // + и -, * и /
            )  
        ):
            output.enqueue(operatorStack.top())
            operatorStack.pop()

        operatorStack.push(token)
    
    if token is '(':
        operatorStack.push(token)
    if token is ')':
        while !operatorStack.isEmpty() AND operatorStack.top() != '(':
            output.enqueue(operatorStack.top())
            operatorStack.pop()

        if operatorStack.isEmpty():
            error: inbalanced parentheses
        
        operatorStack.pop() // выкидываем открывающую скобку

while !operatorStack.isEmpty():
    output.enqueue(operatorStack.top())
    operatorStack.pop()
```

Выходная очередь представляет собой RPN и легко преобразуется в AST.  

## Алгоритм получения значения из выходной очереди

```c++
result = Stack()

while !output.isEmpty():
    token = output.dequeue()
    if token is Number:
        stack.push(token)
    if token is Operator or token is Function:
        // достаем нужное количество операндов со стека
        // применить оператор/функцию к операндам (1-й снятый - последний операнд)
        // положить результат в стек
// в стеке должен остаться один элемент - результат
```

## Общий алгоритм
```c++
tokens = tokenize(expression)
output = shuntingYard(tokens)
result = calculate(output)
```