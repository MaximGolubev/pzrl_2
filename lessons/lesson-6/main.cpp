#include <iostream>

template <typename Value>
class Node
{
public:
    Value value;
    Node* next = nullptr;

    explicit Node(const Value& value, Node* next = nullptr) : value(value), next(next)
    {}

    Node* insertAfter(const Value& value)
    {
        Node* newNode = new Node(value, next);
        next = newNode;
        return next;
    }

    Node* eraseAfter()
    {
        if (next == nullptr || next->next == nullptr)
        {
            return nullptr;
        } 
        
        Node* toRemove = this->next;
        Node* newNext = toRemove->next;

        delete toRemove;

        this->next = newNext;

        return next;
    }
};

template<typename Value>
Node<Value>* reverse(Node<Value>* head)
{
    if (!head || !head->next) 
    {
        return nullptr;
    }

    Node<Value>* prev = nullptr;
    Node<Value>* cur = head;

    while (cur)
    {
        Node<Value>* next = cur->next;
        cur->next = prev;
        prev = cur;
        cur = next;
    }

    return prev;
}


int main()
{
    Node<int> head(0);
    
    constexpr size_t size = 10;

    auto* it = &head;
    for (size_t i = 1; i < size; ++i)
    {
        it = it->insertAfter(i * i);
    }

    std::cout << "Content: ";
    it = &head;
    while (it)
    {
        std::cout << it->value << ", ";
        it = it->next;
    }

    // while ((it = head.eraseAfter()) != nullptr) {}

    auto* newHead = reverse(&head);

    std::cout << std::endl << "Content: ";
    it = newHead;
    while (it)
    {
        std::cout << it->value << ", ";
        it = it->next;
    }
}





// template <typename Value>
// class LinkedList
// {
//     struct Node
//     {
//         Value value
//         Node* next;
//     };

//     class Iterator
//     {
//         Node* _node;
//     public:
//         explicit Iterator(Node node) : _node(node)
//         {}

//         Iterator& operator++()
//         {
//             _node = _node->next;
//             return *this;
//         }

//         Node& operator*()
//         {
//             return *_node;
//         }

//         const Node& operator*() const
//         {
//             return *_node;
//         }

//         Node* operator->()
//         {
//             return _node;
//         }

//         const Node* operator->() const
//         {
//             return _node;
//         }
//     };

//     Node* _head = nullptr;

// public:
//     LinkedList() = default;

//     void insertAfter(Iterator it);

// };