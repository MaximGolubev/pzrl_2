#include "String.h"

#include <algorithm>
#include <cassert>
#include <cstring>

/*
    const char* source = "Hello World!"; // 
    String first(source);
*/

String::String(const char* rawString)
{
    _size = strlen(rawString);
    _data = new char[_size + 1];
    strcpy_s(_data, _size, rawString);
}

String::String(const String& other)
{
    _size = other._size;
    _data = new char[_size + 1];
    strcpy_s(_data, _size,  other._data);
}

String::String(String&& other) noexcept
{
    _size = other._size;
    _data = other._data;

    other._data = nullptr;
    other._size = 0;
}

/*
    String first(...);
    first = second = third = fourth;
    first = first;
*/
String& String::operator=(const String& other)
{
    if (this == &other)
    {
        return *this;
    }

    delete[] _data;

    String buffer(other);
    std::swap(_data, buffer._data);
    std::swap(_size, buffer._size);

    return *this;
}

size_t String::size() const
{
    return _size;
}


/*
    String first("Hello World!");
    std::cout << first.at(0); // H
    std::cout << first.at(1); // e
    std::cout << first.at(2); // l
    first.at(0) = 'r';
    
    const String second("Hello World!");
    second.at(0) = 'r'; // ok

*/
char& String::at(size_t idx)
{
    assert(idx < _size);    // exception: throw std::runtime_error
    return _data[idx];
}

char String::at(size_t idx) const
{
    return at(idx);
}

/*!
    String first("Hello World!");
    std::cout << first[0]; // H
    std::cout << first[1]; // e
    std::cout << first[2]; // l
*/
char& String::operator[](size_t idx)
{
    return at(idx);
}

char String::operator[](size_t idx) const
{
    return at(idx);
}

/*!
    first = "Hello ";
    second = "World";
    first.append(second);  //"Hello World"

    int a = 8;
    int b = 6;
    int c = (a += b);
*/
void String::append(const String& other)
{
    size_t newSize = _size + other._size;
    char* newData = new char[newSize + 1];
    newData[0] = '\0';

    strcat_s(newData, _size, _data);
    strcat_s(newData, other._size, other._data);

    delete[] _data;
    _data = newData;
    _size = newSize;
}

String::~String()
{
    delete[] _data;
}
