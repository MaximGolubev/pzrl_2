#pragma once

#include <iosfwd>

/*!
    char* first = new char[15];
    const char* literal = "Hello World!"; // "Hello World!\0"
    first + literal;
    first = literal;

    #include <cstring>

    strcpy(first, literal);
    strcat(...);

    Data& _dummyData; // error
*/


class String
{
/// first version of layout
private:
    size_t _size;   // 8
    char* _data;    // 8

public:
    String(const char* rawString = nullptr);

    explicit String(const String& other);
    String& operator=(const String& other);

    explicit String(String&& other) noexcept;
    String& operator=(String&& other) noexcept;

    ~String();

    /// size
    size_t size() const;

    /// access to char by index
    char& at(size_t idx);
    char at(size_t idx) const;

    char& operator[](size_t idx);
    char operator[](size_t idx) const;

    /// concatenation
    void append(const String& other);
    String& operator+=(const String& other);

    /*!
        Find index of the first substring

        @substring: searched string
        @pos: search begin index

        @return: index of first occurence if string is found else -1

        Example: 
            String source("concatenation");
            std::cout << source.find("cat"); // 3
            std::cout << source.find("cat", 4); // -1
    */
    long long int find(const String& subString, size_t pos = 0) const;

    /*!
        Erases all characters in range [pos, pos + count)

        Example:
            String source("Hello World!");
            source.erase(4, 7); // "Hell!"
    */
    void erase(size_t pos, size_t count = 1);

    /*!
        Erases all characters from string
    */
    void clear();

    /*!
        Inserts passed string into current string at position
    */
    void insert(const String& insertedString, size_t pos);

// private:
//     union 
//     {
//         struct
//         {
//             char* data;     // 8
//             size_t size;    // 8
//         } largeData;

//         struct
//         {
//             char data[sizeof(largeData) - 1];   // 15
//             unsigned char size;                 // 1
//         } shortData;
//     } _data;

//     bool _isShortString;
};