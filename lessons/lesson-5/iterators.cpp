#include <cassert>
#include <cstdlib>
#include <iostream>

namespace
{
    constexpr size_t Size = 200;
}

class Iterator
{
    int* _ptr;
public:
    explicit Iterator(int* ptr) : _ptr(ptr)
    {}

    int& operator*()
    {
        return *_ptr;
    }

    const int& operator*() const
    {
        return *_ptr;
    }

    int* operator->()
    {
        return _ptr;
    }

    const int* operator->() const
    {
       return _ptr;
    }

    Iterator operator++()
    {
        ++_ptr;
        return *this;
    }

    Iterator operator++(int)
    {
        int* bufPtr = _ptr;
        ++_ptr;
        return Iterator(bufPtr);
    }

    bool operator==(const Iterator& other) const
    {
        return _ptr == other._ptr;
    }

    bool operator!=(const Iterator& other) const
    {
        return !(*this == other);
    }
};

class Integers
{
    int _data[Size];
public:
    Iterator begin()
	{
		return Iterator(&_data[0]);
	}

	Iterator end()
	{
		return Iterator(&_data[Size]);
	}

	void add(size_t idx, int value)
	{
		assert(idx < Size);
		_data[idx] = value;
	}
};

int main()
{
    Integers integers;

    for (auto&& item : integers)
    {
        item = rand() % 256;
    }

    std::cout << std::endl << "Range based for:" << std::endl;
    for (auto&& item : integers)
    {
        std::cout << item << " ";
    }
    return 0;
}
