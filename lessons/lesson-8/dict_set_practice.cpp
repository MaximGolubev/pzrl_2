#include <string>

/*
1. RB-tree
2. BinarySearchTree
3. Hash-table
*/

template<typename Value>
class Vector
{
    Value* _data;
public:
    void pushBack(const Value& value);
};

template<typename Value>
void Vector<Value>::pushBack(const Value& value)
{
    if (_size == 0)
    {
        ...
    }

    --_size;
}

Vector<double> realVec;
Vector<std::string> stringVec;





template <typename Key, typename Value>
class IAssotiativeArray
{
public:
    virtual void add(const Key& key, const Value& value) = 0;
    virtual Value* find(const Key& key) = 0;
    virtual void remove(const Key& key) = 0;

    virtual ~IAssotiativeArray() = default;
};

template<typename Key, typename Value>
class BinarySearchTree : public IAssotiativeArray<Key, Value>
{
    Iterator find(const Key& key) const;
};

// Только уникальные ключи (нет дублирования ключей)
template<typename Key, typename Value>
class Dict
{
    void add(const Key& key, const Value& value)
    {
        if (auto it = _impl.find(key); it != _impl.end())
        {
            it->value = value;
            return;
        }

        _impl.add(key, value);
    }
private:
    BinarySearchTree<Key, Value> _impl;
};

// std::map - RB tree
// std::multimap - RB tree

/*
Dict<std::string, int> dict;
dict.add("Hello", 1);
dict.add("World", 2);
dict.add("Hello", 3);

std::cout << dict; // ("Hello", 3), ("World", 2)
*/

// [1, 2, 1, 5, 7, 2, 5] -> [1, 2, 5, 7]

template<typename Value>
class Set
{
public:
    void add(const Value& value)
    {
        _dict.add(value, value);
    }

    bool contains(const Value& value)
    {
        return _dict.find(value) != _dict.end();
    }

    Set<Value> intersection(const Set& other) const
    {

    }


private:
    Dict<Value, Value> _dict;
};

// std::set - RB tree
// std::multiset - RB tree


// std::unordered_map - Hash table
// std::unordered_set - Hash table


/*
    Найти количество вхождения слов в текст
    Input: text: std::string
    Output: Dict<std::string, unsigned>
*/

// "- Привет, как дела?\n
//  - Да, привет. дела нормально."

Dict<std::string, unsigned int> getFrequency(const std::string& text)
{
    // 1. std::string -> vector<std::string> (text -> words)
    // 1. text.split(' ')

    // 2.1 почистить знаки припенания (for word in words: word = word.strip())
    // 2.2 привести к нижнему регистру: to_lower()

    // 3. пропихнуть в dict

    Dict<std::string, unsigned> result;

    for (auto&& e : words)
    {
        if (auto it = result.find(e); it != result.end())
        {
            it->value += 1;
        }
        else
        {
            result.add(e, 1);
        }
    }

    return result;
}





/*









// <Key, Value>
/*
1. BinarySearchTree
2. RBT
3. Hash-table
*/

// using Key = std::string;
// using Value = unsigned;
/*
template <typename Value>
class Vector
{
    Value* _data = nullptr;
};

Vector<double> realVec;
Vector<std::string> stringVec;


template <typename Key, typename Value>
class IAssotiativeArray
{
    virtual void add(const Key& key, const Value& value) = 0;
    // ...
};

template <typename Key, typename Value>
class Dict
{
    // add(const Key& key, const Value& value) - перезаписывает существующую пару (key, value), если она уже есть в массиве
    // operator[]

    // Iterator find(const Key& key)
    // {
    // {
    // {
    // {
    // {
    //     // ....
    // }

    Value* find(const Key& key)
    {
        // ...
    }


private:
    BinarySearchTree<Key, Value> _BinarySearchTree;
};


template <typename Value>
class Set
{
    void add(const Value& value)
    {
        _dict.add(value, value);
    }

    bool contains(const Value& value)
    {
        return _dict.find(value) != nullptr;
    }

    Set intersection(const Set& other)
    {

    }

private:
    Dict<Value, Value> _dict;
};

*/

/*
1. input - text
2. ouput - word frequency

"Привет, как дела? Да, привет, дела нормально"

("привет", 2)
("как", 1)
...
*/
/*
Dict<std::string, unsigned int> getFrequency(const std::string& text)
{
    // 1. text -> std::vector<std::string>
    // "Привет, как дела? Да, привет, дела нормально" -> ["Привет,", "как", "дела?"...]

    // 2. for word in words: 1. trim(word); 2. to_lower

    // 3. for word in words:
    //      if dict.find(word) != dict.end: // хотим понять, есть элемент в словаре или нет
    //          dict[word] = 1
    //      else:
    //          dict[word] += 1
    
    // 4. return dict
}

Set<std::string> getUnique(const std::string& text)
{
    // ...
}


*/