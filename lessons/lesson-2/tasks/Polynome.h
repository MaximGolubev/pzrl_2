#pragma once


/*!
    @brief: Representation of polynome

    @details: 
    stores coefficients of polynome in raw array where each index is assigned with degree.<br>
    Example: a_0 + a_1 * x^1 + a_2 * x^2 -> [a_0, a_1, a_2]<br>

    If last coefficients are equal to zero (with treshold = 1e-5) then they aren't considered: 
    size/representation getters return result without last zeros
*/
class Polynome
{
    double* _data;
    size_t _size;

public:
    explicit Polynome(double* data, size_t size);

    explicit Polynome(const Polynome& other);
    Polynome& operator=(const Polynome& other);

    Polynome(Polynome&& other) noexcept;
    Polynome& operator=(Polynome&& other) noexcept;

    ~Polynome();

    /*!
        Returns new polynome that is the result of multiplying the current polynome by scalar
    */
    Polynome getMultiplication(double scalar) const;

    /*!
        Multiplies the polynome by scalar (change current polynome) 
    */
    void multiply(double scalar);

    /*!
        Returns new polynome that is the sum of the current and passed polynome
    */
    Polynome sum(const Polynome& other) const;

    /*!
        Add the passed polynome to the current one
    */
    void add(const Polynome& other);

    /*!
        Returns new polynome that is the diiference between the current polynome and passed one
        (*this - other)
    */
    Polynome getSubtraction(const Polynome& other) const;

    /*!
        *this -= other
    */
    void subtract(const Polynome& other);
    
    /*!
        Returns value at point
    */
    double valueAtPoint(double x) const;

    /*!
        Returns deg of polynome
    */
    size_t degree() const;

    /*!
        Returns copy of _data
    */
    double* getRawRepresentation() const;

    /*!
        Returns string representation of the current polynome. 
        Real numbers must be rounded to four decimal places.<br>
        Example:<br> 
            [1.0, 2.01235, 0.0, 1.0, 0.0, 0.0] -> "1.0000 + 2.0123 * x + 1.0000 * x^3" 
    */
    const char* getStringRepresentation() const;
};