# Value Category

## Примитивная модель

Value categories (old model):
* lvalue - имеет адрес;
* rvalue - не lvalue :)

```c++
{
    int number;
    number = 6;            //number - lvalue, 6 - rvalue
    6 = number;            //6 - rvalue, number - lvalue, => ошибка
    (number + 5) = 6;    //(a + 5) - rvalue, 6 - rvalue, => ошибка
}
```

## Актуальная модель

![alt text](https://habrastorage.org/r/w1560/webt/4a/tb/qo/4atbqokizj0rxddb_gcl7siiybo.png)

Value categories:
* lvalue - выражение, которое имеет адрес в памяти, но не является перемещаемым
    * `int a;`
* xvalue — выражение, которое имеет адрес в памяти, а также является перемещаемым
    * `function()`
* prvalue - выражение, которое не имеет адреса в памяти, но является перемещаемым.
    * `nullptr, "Hello World!", 6`

## Преобразования между lvalue и rvalue

1. Конструкции языка, оперирующие значениями объектов, требуют rvalue в качестве аргументов.

2. Неявное преобразование lvalue в rvalue:

```c++
{
    int a = 1;          // a - lvalue
    int b = 2;          // b - lvalue
    a = a + b;          // '+' требует rvalue, a и b неявно конвертируются в rvalue 
                        // и rvalue возвращается в качестве результата
}
```

3. Явное преобразование lvalue в rvalue:    

```c++
{
    int value = 15;
    int* ptr = &value;        //a - lvalue, которое принимается оператором & и кастуется в rvalue
}
```

4. Неявного преобразования rvalue в lvalue не существует.

Но возможны явные преобразования:

```c++
{
    int arr[] = { 1, 2, 3 };
    int* ptr = &arr[0];
    *(ptr + 1) = 10;    //ptr + 1 rvalue, однако *(ptr + 1) уже lvalue
}
```

# Ссылки

Ссылка — тип переменной в C++, который работает как псевдоним другого объекта.
Тот же указатель, который неявно разыменовывается при доступе к значению, на которое он указывает.

Виды ссылок:
* ссылки на неконстантные значения (неконстантные ссылки);
* ссылки на константные значения   (константные ссылки);
* ссылки на временные объекты        (rvalue-ссылки).

1. неконстантные ссылки можно завязать только на lvalue;
2. константные ссылки запрещено изменять, могут быть завязаны и на lvalue, и на rvalue;
3. константные ссылки продлевают время жизни rvalue;
4. ссылки на временные объекты завязываются на rvalue.

```c++
{
    int value = 2;
    int& ref = value;                //объект, на который будет ссылаться ref, обязательно указывать при инициализации
    
    ++ref;                            //value станет равно 6
    
    const int& constRef = value;     //константная ссылка
    ++value;                         //все норм
    ++constRef;                        //нельзя: хоть value и не константа, но ссылка constRef - константа
    
    // result category - rvalue
    int getFive();
    
    const int& otherRef = getFive();    //getFive - временный объект (rvalue)
    std::cout << otherRef;                //но ссылка будет жить до конца области видимости (продление времени жизни 1+2)    
}
```

```c++
{
    struct Node 
    {
        double value;
        Node* prev;
        Node* next;
    };
    
    // result - lvalue
    const double& constGetValueRef(const Node& node) 
    {
        return node.value;
    }
    
    // result - lvalue
    double& getValueRef(Node& node) 
    {
        return node.value;
    }
    
    // result - rvalue
    double getValue(const Node& node) 
    {
        return node.value;
    }
    
    Node node;
    // ...
    
    // error: non-const referencce cannot be initialized with const reference
    {
        double& ref = constGetValueRef(node); 
    }
    // error: rvalue cannot be bound to non-const referencce
    {
        double& ref = getValue(node); 
    }
    // error: rvalue referencce cannot be initialized with lvalue
    {
        double&& ref = getValueRef(node); 
    }
    // ok
    {
        {
            const double& constRef = constGetValueRef(node);
        }
        {
            const double& constRef = getValueRef(node);
        }
        {
            const double& constRef = getValue(node);
        }
        {
            double value = constGetValueRef(node);
        }
        {
            double value = getValue(node);
        }
        {
            double ref = getValueRef(node); 
        }
        
        int&& rRef = getValue(node);
        rRef = 55.5;
    }

    void doSomething(const int& value);
    doSomething(5);
}
```

Ссылки можно использовать и для передачи аргументов в функцию, и для возврата значений (но осторожно):

```c++
{
    int& getReferenceToDynamic(int i) 
    {
        // а тут сюрприз
        int* n = new int(i);
        return *n;
    }
    
    // somewhere in the main
    int& ref = getReferenceToDynamic(5) = 42;
    std::cout << ref; // 42
}
```    

Ссылки и rvalue:
```c++
{
    std::string getHelloStr() 
    {
        return std::string("Hello");
    }
    
    //std::string& myString = getHelloStr(); - error
    std::string&& myString = getHelloStr();
}
```

(!) Ссылки используются для "быстрой" передачи в функции/методы "одиночных" объектов, для передачи массивов объектов используются указатели.

Способ передачи | Вид объектов
--- | ---
указатель | сырые массивы
значение | базовые типы (int, double, ...), когда в функции нужна копия объекта, [если объект занимает в памяти 2-3 машинных слова]
константная ссылка | если в функции хотим только посмотреть на объект
ссылка | если хотите изменить значение объекта в функции
rvalue-ссылка | если хотим передать временный объект и хотим использовать его в функции, также заведомо "прощаемся" с объектом, т.к. функция теперь владелец ресурса


# Копирование и перемещение

**Глубокое копирование** - полное копирование ресурсов одного объекта в другой, включая данные, лежащие по указателям.  
**Перемещение** - передача ресурсов одного объекта другому, при этом перемещаемый объект больше не владеет ресурсами (находится в начальном состоянии).  
  
Для реализации копирования объектов используются **конструктор копирования** и **оператор присваивания копированием**.  
Для реализации перемещения объектов используются **конструктор перемещения** и **оператор присваивания перемещением**.  